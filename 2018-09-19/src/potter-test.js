import {getUniqueCartItemsCount, getGroupedCartItems, getCartGroupsGrandTotal} from './potter';

// Un panier [1, 1, 2, 3, 4, 5]

describe('Potter', () => {

    describe('getUniqueCartItemsCount', () => {
        it('should count unique cart item', () => {
            expect(getUniqueCartItemsCount([])).to.deep.equal({});
            expect(getUniqueCartItemsCount([1])).to.deep.equal({1: 1});
            expect(getUniqueCartItemsCount([1, 1])).to.deep.equal({1: 2});
            expect(getUniqueCartItemsCount([1, 2, 3])).to.deep.equal({1: 1, 2: 1, 3: 1});
        })
    });

    describe('getGroupedCartItems', () => {
        it('compute the total price', () => {
            expect(getGroupedCartItems([])).to.deep.equal([]);
            expect(getGroupedCartItems([1])).to.deep.equal([[1]]);
            expect(getGroupedCartItems([1, 1])).to.deep.equal([[1], [1]]);
            expect(getGroupedCartItems([1, 2, 3])).to.deep.equal([[1, 2, 3]]);
            expect(getGroupedCartItems([1, 2, 2, 3])).to.deep.equal([[1, 2, 3], [2]]);
        })
    });

    describe('getCartGroupsGrandTotal', () => {
        it('compute the groups total price', () => {
            expect(getCartGroupsGrandTotal([])).to.deep.equal(0);
            expect(getCartGroupsGrandTotal([[1]])).to.be.equal(8);
            expect(getCartGroupsGrandTotal([[1], [1]])).to.be.equal(16);
            expect(getCartGroupsGrandTotal([[1, 2, 3]])).to.be.equal(3 * 8 * 0.9);
            expect(getCartGroupsGrandTotal([[1, 2, 3], [2]])).to.be.equal(3 * 8 * 0.9 + 8);
            expect(getCartGroupsGrandTotal([[1, 2, 3, 4], [1, 2, 3, 5]])).to.be.equal(4 * 8 * 0.8 * 2);
        })
    })
});
