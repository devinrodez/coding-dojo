import { getUniqueCartItemsCount, getGroupedCartItems, getCartGroupsGrandTotal } from './potter'

console.log('A super JS app !');
//console.log(getUniqueCartItemsCount([1, 1, 2, 3, 4]));
console.log(getUniqueCartItemsCount([1, 1, 1, 1, 2, 2, 3, 4, 5]));
console.log(getGroupedCartItems([1, 1, 1, 1, 2, 2, 3, 4, 5]));
console.log(getGroupedCartItems([1, 2, 3, 4, 1, 2, 3, 5]));
console.log(getCartGroupsGrandTotal([ [ 1, 2, 3, 4, 5 ], [ 1, 2 ], [ 1 ], [ 1 ] ]));
console.log(getCartGroupsGrandTotal([ [ 1, 2, 3, 4], [ 1, 2 ], [ 1, 5 ], [ 1 ] ]));