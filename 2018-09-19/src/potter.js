export const REGULAR_PRICE = 8;
export const DISCOUNTS = {
    1: 0,
    2: 5,
    3: 10,
    4: 20,
    5: 25
};

const cart = [1, 1, 2, 2, 3, 3, 4, 5];
const grouped = {
    1: [],
    2: [],
    3: [1, 2, 3], // 3 * 10 = 30
    4: [],
    5: [1, 2, 3, 4, 5] // 5 * 25 = 125 ==== 155
};
const grouped2 = {
    1: [],
    2: [],
    3: [],
    4: [1, 2, 3, 4, 1, 2, 3, 5],//8*20 = 160 ==== 160
    5: []
};


export function getUniqueCartItemsCount(products) {
    return products.reduce((acc, i) => {
        if (acc[i]) {
            ++acc[i];
        } else {
            acc[i] = 1;
        }
        return acc;
    }, {});
}

export function getGroupedCartItems(products) {
    const counts = getUniqueCartItemsCount(products);
    let currentGroup = [];
    for (let bookNumber in counts) {
        if (counts.hasOwnProperty(bookNumber)) {
            for (let i = 0, count = counts[bookNumber]; i < count; ++i) {
                if (!currentGroup[i]) {
                    currentGroup[i] = [];
                }
                currentGroup[i].push(1 * bookNumber);
            }
        }
    }

    return currentGroup;

}
export function getMaybeOptimizedGroupedCartItems(products) {
    const counts = getUniqueCartItemsCount(products);
    const maxGroupNumber = Object.values(counts).reduce((acc, i) => {
        return i > acc ? i : acc;
    }, 0);
    const numDifferentItems = Object.keys(counts).length;
    const numProducts = products.length;
    const maxByGroup = numProducts / numDifferentItems;
    const maxDiscount = Math.min(maxByGroup, Object.keys(DISCOUNTS).pop());

    let ratio = numProducts / maxDiscount;
    let ratioCeiled = Math.ceil(ratio);
    var optimumDiscount = numProducts / ratioCeiled;

    let currentGroup = [];
    for (let bookNumber in counts) {
        if (counts.hasOwnProperty(bookNumber)) {
            for (let i = 0, count = counts[bookNumber]; i < count; ++i) {
                if (!currentGroup[i]) {
                    currentGroup[i] = [];
                }
                currentGroup[i].push(1 * bookNumber);
            }
        }
    }

    return currentGroup;

}

export function getCartGroupsGrandTotal(groups) {
    return groups.reduce((acc, groupItems) => {
        let discount = 1 - DISCOUNTS[groupItems.length] / 100;

        return acc + REGULAR_PRICE * groupItems.length * discount;
    }, 0);
}

export function getGrandTotal(products) {
    var groups = getGroupedCartItems(products);

    return getCartGroupsGrandTotal(groups);
}

