import { isMultipleOf, filterBuilder, Converter } from './fizzbuzz';

describe('FizzBuzz', () => {
  describe('isMultipleOf', () => {
    it('should exists', () => {
      expect(isMultipleOf).to.be.a('function');
    });
    it('should return true when param is multiple of 3', () => {
      expect(isMultipleOf(3, 5)).to.be.false;
      expect(isMultipleOf(3, 12)).to.be.true;
      expect(isMultipleOf(3, 0)).to.be.true;
      expect(isMultipleOf(3, 1)).to.be.false;
      expect(isMultipleOf(3, 15)).to.be.true;
    });
    it('should return true when param is multiple of 5', () => {
      expect(isMultipleOf(5, 5)).to.be.true;
      expect(isMultipleOf(5, 12)).to.be.false;
      expect(isMultipleOf(5, 0)).to.be.true;
      expect(isMultipleOf(5, 1)).to.be.false;
      expect(isMultipleOf(5, 15)).to.be.true;
    });
  });
  describe('filterBuilder', () => {
    it('should exists', () => {
      expect(filterBuilder).to.be.a('function');
    });
    it('should create a fizzFilter object', () => {
      const filter = filterBuilder(3, 'Fizz');
      expect(filter).to.be.an('Object');
      expect(filter).to.have.property('multiplier', 3);
      expect(filter).to.have.property('execute').that.is.a('function');
    });
    it('should convert 3 multiples to "Fizz"', () => {
      const fizzFilter = filterBuilder(3, 'Fizz');
      expect(fizzFilter.multiplier).to.be.equal(3);
      expect(fizzFilter.execute(15)).to.be.equal('Fizz');
      expect(fizzFilter.execute(30)).to.be.equal('Fizz');
      expect(fizzFilter.execute(10)).to.be.equal(10);
    });
    it('should convert 5 multiples to "Buzz"', () => {
      const buzzFilter = filterBuilder(5, 'Buzz');
      expect(buzzFilter.multiplier).to.be.equal(5);
      expect(buzzFilter.execute(15)).to.be.equal('Buzz');
      expect(buzzFilter.execute(30)).to.be.equal('Buzz');
      expect(buzzFilter.execute(11)).to.be.equal(11);
    });
  });
  describe('Converter', () => {
    const filters = [
      filterBuilder(3, 'Fizz'),
      filterBuilder(15, 'FizzBuzz'),
      filterBuilder(5, 'Buzz'),
    ];
    it('should exists', () => {
      expect(Converter).to.be.a('function');
    });
    describe('getFilters', () => {
      it('should return a sorted array of filters', () => {
        const converter = new Converter(filters);
        expect(converter.getFilters().map(f => f.multiplier)).to.deep.equal([15, 5, 3]);
      });
    });
    describe('convert', () => {
      it('should convert passed-in number parameter', () => {
        const converter = new Converter(filters);
        expect(converter.convert(1)).to.be.equal(1);
        expect(converter.convert(9)).to.be.equal('Fizz');
        expect(converter.convert(10)).to.be.equal('Buzz');
        expect(converter.convert(30)).to.be.equal('FizzBuzz');
        expect(converter.convert(34)).to.be.equal(34);
      });
    });
  });
});
